﻿using LogAndExceptionTask.HelperService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAndExceptionTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogtestsController : ControllerBase
    {
        IConfiguration configure;
        public LogtestsController(IConfiguration configuration)
        {
            configure = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {

            string query = @"select StudentID, StudentName, GendId, DepId, format(DateOfBirth, 'dd/MM/yyyy') as DOB, MarksPercentage from dbo.Student";

            JsonResult data = null;
            try
            {
                data = new UseMeToHelpYou(configure).ExecuteGetQuery(query);
                
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string verb = Request.Method;
                string _query = Request.QueryString.Value;
                ICollection<string> headers = Request.Headers.Keys;

                UseMeToHelpYou.ApiLogs(URL, verb, _query, headers);

                return Ok(data);
            }
            catch (Exception e)
            {
                string typeError = e.GetType().Name.ToString();
                string msgError = e.Message.ToString();
                UseMeToHelpYou.ExceptionMethod(typeError, msgError);
                return StatusCode(500, e);

            }
        }
    }
}