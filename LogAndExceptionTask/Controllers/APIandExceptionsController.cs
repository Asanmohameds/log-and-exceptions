﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAndExceptionTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APIandExceptionsController : ControllerBase
    {

        IConfiguration _configure;
 
        public APIandExceptionsController(IConfiguration configuration)
        {
            _configure = configuration;
        }

        [HttpGet("[action]/{date}")]
        public IActionResult ExceptionLog(string date)
        {
            string path = @"E:\FTS Tasks\C# FTS tasks\Log and Exception task\LogAndExceptionTask\LogAndExceptionTask\Logs\ExceptionLogs\ExceptionMessages- " + date + ".txt";

            string[] readTexts = System.IO.File.ReadAllLines(path);
            //foreach(string text in readTexts)
            //{
            //    Console.WriteLine(text);
            //    Console.ReadKey();
            //}

            return Ok(string.Join("\n", readTexts));
        }



        [HttpGet("[action]/{date}")]
        public IActionResult ApiLog(string date)
        {
            string path = @"E:\FTS Tasks\C# FTS tasks\Log and Exception task\LogAndExceptionTask\LogAndExceptionTask\Logs\APILogs\APILogdetails - " + date + ".txt";

            string[] readTexts = System.IO.File.ReadAllLines(path);

            return Ok(string.Join("\n", readTexts));
        }
    }
}
