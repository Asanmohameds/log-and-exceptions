﻿using LogAndExceptionTask.HelperService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LogAndExceptionTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        IConfiguration configure;
        public StudentsController(IConfiguration configuration)
        {
            configure = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {


                string query = @"select StudentID, StudentName, GendId, DepId, format(DateOfBirth, 'dd/MM/yyyy') as DOB, MarksPercentage from dbo.Student";

                JsonResult data = null;
                try
                {
                    data = new UseMeToHelpYou(configure).ExecuteGetQuery(query);
                    return Ok(data);
                    //throw new Exception("File founded");
                }
                catch (Exception e)
                {
                    string filePath = @"E:\FTS Tasks\C# FTS tasks\Log and Exception task\LogsMessage\ExceptionLog.txt";
                    if (System.IO.File.Exists(filePath))
                    {
                        StreamWriter sw = new StreamWriter(filePath);
                        sw.Write(e.GetType().Name);
                        sw.WriteLine();
                        sw.Write(e.Message);
                        Console.WriteLine("There is a problem, Please try later");
                        return BadRequest(e);
                    }
                    else
                    {
                        throw new FileNotFoundException(filePath + "is not present", e);
                    }
               
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Current Exception = {0}", ex.GetType().Name);
                Console.WriteLine("Inner Exception = {0}", ex.InnerException.GetType().Name);
                return BadRequest(ex);
            }

        }
    }
}
