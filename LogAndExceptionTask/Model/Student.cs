﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAndExceptionTask.Model
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        public DateTime DateOfBirth { get; set; }
        public int MarksPercentage { get; set; }

        public int GendId { get; set; }
        public int DepId { get; set; }
    }
}

