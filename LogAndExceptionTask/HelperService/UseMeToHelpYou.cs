﻿using System;
using System.Collections.Generic;
using System.Data;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;

namespace LogAndExceptionTask.HelperService
{
    public class UseMeToHelpYou
    {
        IConfiguration _configure;
        public UseMeToHelpYou(IConfiguration configuration)
        {
            _configure = configuration;
        }

        public JsonResult ExecuteGetQuery(string query)
        {
            string serverPath = _configure.GetConnectionString("Database");
            //** need to install system.data.sqlclient

            SqlDataReader reader;

            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(serverPath))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    reader = cmd.ExecuteReader();
                    table.Load(reader);
                    con.Close();
                    reader.Close();
                }
                return new JsonResult(table);

            }
        }


        public static void ExceptionMethod(string typeError, string msgError)
        {
            DateTime dateForFile = DateTime.Now;
            string dateAloneInString = dateForFile.ToString("dd-MM-yyyy");
            string dateWithTimeInString = dateForFile.ToString("dd-MM-yyyy hh:mm:ss:tt");

            string path = @"E:\FTS Tasks\C# FTS tasks\Log and Exception task\LogAndExceptionTask\LogAndExceptionTask\Logs\ExceptionLogs\ExceptionMessages- " + dateAloneInString + ".txt";

            if (!System.IO.File.Exists(path))
            {
                // Create a file and update the error message if file not exists.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine("Date and Time: " + dateWithTimeInString);
                    sw.WriteLine("Type Of Error: " + typeError);
                    sw.WriteLine("Error Message: " + msgError);
                    sw.WriteLine("**************End of Error Message***********");
                    sw.WriteLine();
                }
            }
            else
            {
                // Append the error message to file which exists already...
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine("Date and Time: " + dateWithTimeInString);
                    sw.WriteLine("Type Of Error: " + typeError);
                    sw.WriteLine("Error Message: " + msgError);
                    sw.WriteLine("**************End of Error Message***********");
                    sw.WriteLine();
                }
            }

        }


        public static void ApiLogs(string URL, string verb, string _query, ICollection<string> headers)
        {
            DateTime dateForFile = DateTime.Now;
            string dateAloneInString = dateForFile.ToString("dd-MM-yyyy");
            string dateWithTimeInString = dateForFile.ToString("dd-MM-yyyy hh:mm:ss:tt");

            string path = @"E:\FTS Tasks\C# FTS tasks\Log and Exception task\LogAndExceptionTask\LogAndExceptionTask\Logs\APILogs\APILogdetails - " + dateAloneInString + ".txt";

            if (!System.IO.File.Exists(path))
            {
                // Create a file and update the API Log details if file not exists.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine("Date and Time: " + dateWithTimeInString);
                    sw.WriteLine("URL: " + URL);
                    sw.WriteLine("Verb: " + verb);
                    sw.WriteLine("Query: " + _query);
                    sw.WriteLine("Headers: ");
                    foreach(var data in headers)
                    {
                        sw.WriteLine("key= " + data);
                    }
                    sw.WriteLine("**************End of API Log Message***********");
                    sw.WriteLine();
                }
            }
            else
            {
                // Append the Log message to file which exists already...
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine("Date and Time: " + dateWithTimeInString);
                    sw.WriteLine("URL: " + URL);
                    sw.WriteLine("Verb: " + verb);
                    sw.WriteLine("Query: " + _query);
                    sw.WriteLine("Headers: ");
                    foreach (var data in headers)
                    {
                        sw.WriteLine("key= " + data);
                    }
                    sw.WriteLine("**************End of API Log Message***********");
                    sw.WriteLine();
                }
            }
        }

    }
}
